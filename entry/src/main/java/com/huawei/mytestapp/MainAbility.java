/*
 *    Copyright 2015 Xieyupeng520
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.huawei.mytestapp;


import com.example.explosopnlibrary.explosion.ExplosionField;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.global.resource.ResourceManager;
import ohos.media.image.Image;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        DirectionalLayout directionalLayout = (DirectionalLayout) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_mian_activity, null, false);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(new RgbColor(255,239,213));
        directionalLayout.setBackground(element);
        super.setUIContent(directionalLayout);
        ExplosionField explosionField = new ExplosionField(this);
        explosionField.addListener((ComponentContainer) findComponentById(ResourceTable.Id_root));
    }

}
