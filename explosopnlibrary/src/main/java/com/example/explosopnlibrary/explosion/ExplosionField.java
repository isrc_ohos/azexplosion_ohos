/*
 *    Copyright 2015 Xieyupeng520
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.example.explosopnlibrary.explosion;
//import android.animation.Animator;
//import android.animation.AnimatorListenerAdapter;
//import android.app.Activity;
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.Canvas;
//import android.graphics.Rect;
//import android.util.AttributeSet;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import com.huawei.mytestapp.ResourceTable;

//import com.huawei.mytestapp.ResourceTable;
import com.example.explosopnlibrary.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.io.InputStream;
import java.util.ArrayList;
//import com.huawei.mytestproject.ResourceTable;
//import ohos.media.image.common.Rect;

/**
 * Created by azz on 15/11/19.
 */
public class ExplosionField extends Component {
    private static final String TAG = "ExplosionField";
    public final float DENSITY = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getAttributes().densityPixels;
    public int dp2px(int dp) {
        return Math.round(dp * DENSITY);
    }
    private static final Canvas mCanvas = new Canvas();
    private ArrayList<ExplosionAnimator> explosionAnimators;
    private ClickedListener onClickListener;
    private boolean endFlag = true;
    public ExplosionField(Context context) {
        super(context);
        init();
    }
    public ExplosionField(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }
    private void init() {
        explosionAnimators = new ArrayList<ExplosionAnimator>();
        attach2Activity((Ability) getContext());
    }
    /**
     * 爆破
     * @param view 使得该view爆破
     */

    public void explode(final Component view) {
        //获取view在屏幕上的绝对坐标
        int[] positions = view.getLocationOnScreen();
        //修正因为状态栏导致的错位
        positions[1] = positions[1] - 350;
       HiLogLabel logLabel = new HiLogLabel(3,0,"qqqqqqq:");
        HiLog.info(logLabel,"positions[1]="+positions[1]);
        Rect rect = view.getComponentPosition();
        HiLog.info(logLabel,"rect="+rect.left+"/"+rect.top+"/"+rect.right+"/"+rect.bottom);
        final ExplosionAnimator animator = new ExplosionAnimator(this, createBitmapFromView(view), rect);
        explosionAnimators.add(animator);
        //爆炸值动画
        if (endFlag) {
            addDrawTask(new DrawTask() {
                @Override
                public void onDraw(Component component, Canvas canvas) {
                    canvas.save();
                    canvas.translate(0,positions[1]);


                    for (ExplosionAnimator animator : explosionAnimators) {
                        animator.draw(canvas);
                    }

                    canvas.restore();

                }
            }, DrawTask.BETWEEN_CONTENT_AND_FOREGROUND);
            //闪现属性动画
            animator.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                    endFlag = false;
                    view.createAnimatorProperty().alpha(0f).setDuration(150).start();
                }

                @Override
                public void onStop(Animator animator) {
                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    view.createAnimatorProperty().alpha(1f).setDuration(150).start();
                    explosionAnimators.remove(animator);
                    animator = null;
                    endFlag = true;
                    view.addDrawTask(new DrawTask() {
                        @Override
                        public void onDraw(Component component, Canvas canvas) {
                        }
                    });
                }
                @Override
                public void onPause(Animator animator) {
                }
                @Override
                public void onResume(Animator animator) {
                }
            });
            animator.start();
        }
    }
       int anInt=2;
    private PixelMap createBitmapFromView(Component view) {
        //Default
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(100,100);
        //创建位图对象
        PixelMap bitmap = PixelMap.create(options);

        if(view.getName().equals("Id_qq")){
            bitmap =getPixelMap(ResourceTable.Media_qq);
        }
        if(view.getName().equals("Id_qzone"))
            bitmap =getPixelMap(ResourceTable.Media_qzone);
        if(view.getName().equals("Id_vx"))
            bitmap =getPixelMap(ResourceTable.Media_vx);
        if(view.getName().equals("Id_wb"))
            bitmap =getPixelMap(ResourceTable.Media_wb);
        if(view.getName().equals("Id_tb"))
            bitmap =getPixelMap(ResourceTable.Media_tb);
        if(view.getName().equals("Id_baidu_map"))
            bitmap =getPixelMap(ResourceTable.Media_baidu_map);
        if(view.getName().equals("Id_gaode_map"))
            bitmap =getPixelMap(ResourceTable.Media_gaode_map);
        if(view.getName().equals("Id_iqiyi"))
            bitmap =getPixelMap(ResourceTable.Media_iqiyi);
        if(view.getName().equals("Id_changba"))
            bitmap =getPixelMap(ResourceTable.Media_changba);
        if(view.getName().equals("Id_jd"))
            bitmap =getPixelMap(ResourceTable.Media_jd);
        if(view.getName().equals("Id_qq_music"))
            bitmap =getPixelMap(ResourceTable.Media_qq_music);
        if(view.getName().equals("Id_lm"))
            bitmap =getPixelMap(ResourceTable.Media_lm);

        return bitmap;
    }
    private PixelMap getPixelMap(int resId) {
        InputStream drawableInputStream = null;
        try {
            drawableInputStream = getResourceManager().getResource(resId);
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            sourceOptions.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(drawableInputStream, null);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
//            decodingOptions.desiredSize = new Size(5, 5);
            decodingOptions.desiredPixelFormat = PixelFormat.ARGB_8888;
            PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
            return pixelMap;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try{
                if (drawableInputStream != null){
                    drawableInputStream.close();
                }
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    //给Activity加上全屏覆盖的ExplosionField

    private void attach2Activity(Ability activity) {
//        ComponentContainer mView = (ComponentContainer) activity.findComponentById(ResourceTable.Id_root);
//        Class<? extends ComponentParent> rootView = mView.getComponentParent().getClass();
        ComponentContainer rootView = (ComponentContainer) activity.findComponentById(ResourceTable.Id_group1).getComponentParent().getComponentParent();
        //ComponentContainer rootView = (ComponentContainer) activity.findComponentById();
        ComponentContainer.LayoutConfig lp = new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
//        //Component a=findComponentById(ResourceTable.Id_qq);
        rootView.addComponent(this, lp);
    }
    /**
     * 希望谁有破碎效果，就给谁加Listener
     * @param view 可以是ViewGroup
     */
    public void addListener(Component view) {
        if (view instanceof ComponentContainer) {
            ComponentContainer viewGroup = (ComponentContainer) view;
            int count = viewGroup.getChildCount();
            for (int i = 0 ; i < count; i++) {
                addListener(viewGroup.getComponentAt(i));
            }
        } else {
            view.setClickable(true);
            view.setClickedListener(getOnClickListener());
        }
    }
    private ClickedListener getOnClickListener() {
        if (null == onClickListener) {
            onClickListener = new ClickedListener() {
                @Override
                public void onClick(Component component) {
                    ExplosionField.this.explode(component);
                }
            };
        }
        return onClickListener;
    }
}
