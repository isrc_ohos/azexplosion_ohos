/*
 *    Copyright 2015 Xieyupeng520
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.example.explosopnlibrary.explosion;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Position;
//import ohos.media.image.common.Rect;

/**
 * Created by azz on 15/11/19.
 */
public class ExplosionAnimator extends AnimatorValue implements AnimatorValue.ValueUpdateListener {
    //默认时间间隔
    public static final int DEFAULT_DURATION = 1500;
    //粒子
    private Particle[][] mParticles;
    //画笔
    private Paint mPaint;
    //组件
    private Component mContainer;
    private float myvalue =0;
    //粒子动画
    public ExplosionAnimator(Component view, PixelMap bitmap, Rect bound) {
        mPaint = new Paint();
        mContainer = view;
        setDuration(DEFAULT_DURATION);
        setValueUpdateListener(this);
        mParticles = generateParticles(bitmap, bound);
    }
    //生成粒子
    private Particle[][] generateParticles(PixelMap bitmap, Rect bound) {
        HiLogLabel hiLogLabel = new HiLogLabel(3,0,"qqqqqqq:");
        int w = bound.getWidth();
        int h = bound.getHeight();
        int partW_Count = w / Particle.PART_WH; //横向个数
        int partH_Count = h / Particle.PART_WH; //竖向个数
        HiLog.info(hiLogLabel,"bound width"+w+"bound height"+h);
        int bitmap_part_w = bitmap.getImageInfo().size.width / partW_Count;
        int bitmap_part_h = bitmap.getImageInfo().size.height / partH_Count;
        HiLog.info(hiLogLabel,"bitmap width"+bitmap.getImageInfo().size.width+"bitmap width"+bitmap.getImageInfo().size.height);
        Particle[][] particles = new Particle[partH_Count][partW_Count];
        Point point = null;
        for (int row = 0; row < partH_Count; row ++) { //行
            for (int column = 0; column < partW_Count; column ++) { //列
                //取得当前粒子所在位置的颜色
                int color = bitmap.readPixel(new Position(column* bitmap_part_w, row * bitmap_part_h));
                HiLog.info(hiLogLabel,"color"+ Color.WHITE.getValue());
                point = new Point(column, row); //x是列，y是行
                particles[row][column] = Particle.generateParticle(color, bound, point);
            }
        }
        return particles;
    }
    public void draw(Canvas canvas) {
        //动画结束时停止
        if(!isRunning()) {
            return;
        }
        for (Particle[] particle : mParticles) {
            for (Particle p : particle) {
                p.advance(myvalue);
                mPaint.setColor(new Color(p.color));
                //只是这样设置，透明色会显示为黑色
                mPaint.setAlpha((int) (p.alpha));
                canvas.drawCircle(p.cx, p.cy, p.radius, mPaint);
            }
        }
        mContainer.invalidate();
    }
    @Override
    public void start() {
        super.start();
        mContainer.invalidate();
    }
    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        myvalue = v ;
        mContainer.invalidate();
    }
}
