/*
 *    Copyright 2015 Xieyupeng520
 *    Copyright 2021 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.example.explosopnlibrary.explosion;

import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;

import java.util.Random;

//import ohos.media.image.common.Rect;

/**
 * Created hupengda on 2021/02/10.
 * 爆破粒子
 */
public class Particle {
    //默认小球宽高
    public static final int PART_WH = 5;
    //实际的值（可变）
    //center x of circle
    float cx;//粒子圆心x
    //center y of circle
    float cy;//粒子圆心y
    float radius;//粒子半径
    int color;//颜色
    float alpha;//透明度
    Rect mBound;
    static Random random = new Random();
    public static Particle generateParticle(int color, Rect bound, Point point) {
        //行是高
        int row =  point.getPointYToInt();
        //列是宽
        int column = point.getPointXToInt();
        Particle particle = new Particle();
        particle.mBound = bound;
        //粒子颜色
        particle.color = color;
        //透明度
        particle.alpha = 1f;
        //粒子半径
        particle.radius = PART_WH;
        particle.cx = bound.left+ PART_WH * column;
        particle.cy = bound.top + PART_WH * row;
        return particle;
    }
    public void advance(float factor) {
        cx = cx + factor * random.nextInt(mBound.getWidth()) * (random.nextFloat() -0.5f);
        cy = cy + factor * random.nextInt(mBound.getHeight() / 2);
        radius = radius - factor * random.nextInt(2);
        alpha = (1f - factor) * (1 + random.nextFloat());
    }
}
